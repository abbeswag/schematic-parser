package main

import (
	"bytes"
	"fmt"
	"github.com/json-iterator/go"
	"github.com/ppacher/nbt"
	"io/ioutil"
	"mc/id"
	"os"
	"strconv"
	"strings"
)

var FILENAME string

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type Dire struct {
	StateArray []int `json:"stateIntArray"`
	Dim int `json:"dim"`
	PosArray []int `json:"posIntArray"`
	StartPos Vec3 `json:"startPos"`
	EndPos Vec3`json:"endPos"`
	MapIntState []MapSlot`json:"mapIntState"`
}

//just so it marshals the mapStates properly.
type ExtraS string

func (s ExtraS) MarshalJSON() ([]byte, error) {
	return []byte(s), nil
}

func (s * ExtraS) UnmarshalJSON(data []byte) error {
	return nil
}

const AIRID = 0

type MapSlot struct {
	ID ExtraS `json:"mapSlot"`
	MapState  MapState `json:"mapState"`
	RetID uint8 `json:"-"`
	BlockID uint8 `json:"-"`
}

type MapState struct {
	Name string
	Properties map[string]string `json:"Properties,omitempty"`
}

type Vec3 struct {
	X int
	Y int
	Z int
}

func main() {
	//Prefetch minecraft IDS. Cache in the future
	id.GetIds()

	args := os.Args[1:]

	if len(args) == 0 {
		FILENAME = "schem.schematic"
	} else {
		FILENAME = args[0]
	}

	createIntegerPosition(1,3,5)
	b := LoadFile(FILENAME)
	if len(b) == 0 {
		panic("couldnt read file")
	}

	tag := parseNbt(b)

	d := HandleSchematic(tag)

	str, _ := json.Marshal(d)


	fmt.Println(processOutput(string(str)))
}

func (d *Dire) GetStateID(b byte, dd byte) byte {
	for _,v := range d.MapIntState {
		if v.BlockID == b {
			return v.RetID
		}
	}
	m := MapSlot{}
	m.RetID = uint8(len(d.MapIntState)+1)
	tStr := fmt.Sprint(strconv.Itoa(int(m.RetID))) + "s"
	m.ID = ExtraS(tStr)
	bl := id.BlockID{}
	bl.ID = b
	bl.Data = dd
	block := id.BlockIDS[bl]
	m.MapState.Name = block.Name
	if m.MapState.Name == "" {
		bl.Data = 0
		block = id.BlockIDS[bl]
		m.MapState.Name = block.Name
	}
	p := id.BlockIDS[bl].Properties
	if len(p) > 0 && p != nil {
		m.MapState.Properties = id.BlockIDS[bl].Properties
	}
	m.BlockID = b
	d.MapIntState = append(d.MapIntState, m)
	return m.RetID
}

func HandleSchematic(t *nbt.CompoundTag) Dire {
	d := Dire{EndPos: Vec3{X: 0, Y: 0, Z: 0}, StartPos: Vec3{X: ParseIntTag(t.Tags["Width"]), Y: ParseIntTag(t.Tags["Height"]), Z: ParseIntTag(t.Tags["Length"])}, Dim:0}
	d.parseBlock(t.Tags["Blocks"], t.Tags["Data"])
	return d
}

func (d *Dire) parseBlock(blockData nbt.Tag, stateData nbt.Tag) {
	bd := blockData.(*nbt.ByteArray)
	sd := stateData.(*nbt.ByteArray)

	var w,l,h int

	for w  = 0; w < d.StartPos.X; w++ {
		for l = 0; l < d.StartPos.Z; l++ {
			for h = 0; h < d.StartPos.Y; h++ {
				index := (h*d.StartPos.Z + l)*d.StartPos.X + w
				if index > len(bd.Value) {
					panic("out of bounds")
				}
				if bd.Value[index] == 0 {
					continue
				}
				d.PosArray = append(d.PosArray, createIntegerPosition(w,h,l))
				bId := bd.Value[index]
				c := sd.Value[index]


				sData := c & ((1 << 4)-1)
				id := d.GetStateID(bId, sData)
				d.StateArray = append(d.StateArray, int(id))
			}
		}
	}
	print("")
}

func createIntegerPosition(x,y,z int) int {
	//gotta use dem bytes
	xx := uint8(x & 0xff)
	yy := uint8(y & 0xff)
	zz := uint8(z & 0xff)

	originalValue := 0

	updatedValue := originalValue & ^(0xFF << (0 << 3)) | (int(xx) << (0 << 3))
	updatedValue = updatedValue & ^(0xFF << (1 << 3)) | (int(yy) << (1 << 3))
	updatedValue = updatedValue & ^(0xFF << (2 << 3)) | (int(zz) << (2 << 3))

	return updatedValue
}

func ParseIntTag(t nbt.Tag) int {
	val, ok := t.(*nbt.ShortTag)
	if ok {
		return int(val.Value)
	}
	print(val)
	panic("error int")
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func LoadFile(name string) []uint8 {
	b, err := id.Unzip(name)
	if err.Error() == "zip: not a valid zip file" {
		b, err := ioutil.ReadFile(name)
		check(err)
		return b
	}
	check(err)
	return b
}

func parseNbt(b []uint8) *nbt.CompoundTag {
	r := bytes.NewReader(b)
	m, err := nbt.ReadNamedTag(r)
	check(err)
	return m.(*nbt.CompoundTag)
}
//lame hack, not sure if needed but better safe than sorry
func processOutput(input string) string {
	val := strings.Replace(input, "\"stateIntArray\":[", "stateIntArray:[I;",1)
	val = strings.Replace(val, "\"posIntArray\":[", "posIntArray:[I;",1)
	val = strings.Replace(val, "\"Name\"", "Name",-1)
	val = strings.Replace(val, "\"X\"", "X",-1)
	val = strings.Replace(val, "\"Y\"", "Y",-1)
	val = strings.Replace(val, "\"Z\"", "Z",-1)
	val = strings.Replace(val, "\"mapState\"", "mapState",-1)
	val = strings.Replace(val, "\"mapIntState\"", "mapIntState",-1)

	val = strings.Replace(val, "\"endPos\"", "endPos",-1)
	val = strings.Replace(val, "\"startPos\"", "startPos",-1)
	val = strings.Replace(val, "\"dim\"", "dim",-1)
	val = strings.Replace(val, "\"mapSlot\"", "mapSlot",-1)

	return val
}