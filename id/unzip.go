package id

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
)

func Unzip(src string) ([]byte, error) {
	var b []byte


	r, err := zip.OpenReader(src)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	for k, f := range r.File {
		if k > 1 {
			fmt.Println("Too many files in zip folder, ignoring.")
		}
		rc, err := f.Open()
		if err != nil {
			return nil, err
		}
		defer rc.Close()


		outFile := bytes.NewBuffer(b)
		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop

		if err != nil {
			return nil, err
		}

		}
	return b, nil
}